#!/bin/sh -e

VERSION=$2
TAR=../java-xmlbuilder_$VERSION.orig.tar.gz
DIR=java-xmlbuilder-$VERSION
TAG=$(echo "java-xmlbuilder-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export http://java-xmlbuilder.googlecode.com/svn/tags/${TAG}/ $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
